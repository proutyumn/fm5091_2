%A demonstration of the switch control flow statement
function [A]=FM5091_2_switch(x)

    switch(x) %check the value x to see if it matches conditions below...
        case 5 %if it's equal to 5...
            A = 10;
        case 10 %equal to 10...
            A = 50;
        case 30
            A = 100;
        otherwise
            A = 1000;
    end
    
end