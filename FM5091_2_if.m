%A demonstration of the if control flow statement
function [A]=FM5091_2_if(x)

    if(x == 5) %test whether x is equal to 5
        A = 10;
    else %if x is equal to any number other than 5...
        A = 50;
    end

end