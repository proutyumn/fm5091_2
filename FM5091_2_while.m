%A demonstration of the while control flow statement
function [A]=FM5091_2_while(x)

    i = 0; %make some kind of counter
    while(i ~= x) %while i is not equal to x
        i = i + 1 %increment i
    end

end