%A demonstration of the for control flow statement
function [A]=FM5091_2_for(x)

    G = 100:1000; %define an arbitrary vector to read

    for i = 1:x %count from 1 to the user input
        G(i) %output value of vector G at index i
    end

end